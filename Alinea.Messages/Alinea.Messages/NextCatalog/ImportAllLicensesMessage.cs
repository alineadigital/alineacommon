﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alinea.Messages.NextCatalog
{
	public class ImportAllLicensesMessage
	{
		public Guid AccountId { get; set; }
	}
}
